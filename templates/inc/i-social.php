<div class="social">
	<a href="#" title="Follow O'Brien's Boat Tours on Instagram" class="social-ig" rel="external">Follow O'Brien's Boat Tours on Instagram</a>
	<a href="#" title="Like O'Brien's Boat Tours on Facebook" class="social-fb" rel="external">Like O'Brien's Boat Tours on Facebook</a>
	<a href="#" title="Follow O'Brien's Boat Tours on Twitter" class="social-tw" rel="external">Follow O'Brien's Boat Tours on Twitter</a>
	<a href="#" title="Read Reviews of O'Brien's Boat Tours on TripAdviser" class="obriens-f ob-tripadvisor" rel="external">Read Reviews of O'Brien's Boat Tours on TripAdviser</a>
</div><!-- .social -->
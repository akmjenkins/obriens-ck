<div class="nav-aside-wrap">
	<div class="nav-aside">
		<div class="nav-aside-content">
			<div class="toggle-nav-wrap">
				<button class="toggle-nav">
					<span>&nbsp;</span> Menu
				</button><!-- .toggle-nav -->
			</div><!-- .toggle-nav-wrap -->
			
			<button class="t-fa-abs fa-info-circle">Information</button>
			
			<a href="#" class="t-fa fa-sitemap">Sitemap</a>	
		</div><!-- .nav-aside-content -->
	</div><!-- .nav-aside -->
</div><!-- .nav-aside-wrap -->
	
<div class="nav">

	<span class="nav-bg"></span>

	<div>
		<a href="#" class="nav-logo">
			<img src="../assets/images/obriens-boat-tours.png" alt="O'Briens Boat Tours Logo">
		</a>
	
		<div class="nav-list-wrap">
	
			<nav>
				<ul>
					<li class="tours-link">
						<a href="#">Boat Tours</a>
						
						<div class="tours-drop">
							<ul>
								<li>
									<div class="package">
									
										<a href="#" class="title">Award Winning Boat Tour</a>
										
										<span class="package-img" data-src="../assets/images/temp/gallery/gallery-4.jpg"></span>
										
										<p>
											Lorem ipsum dolor sit amet, consectetur
											adipiscing elit. Phasellus dignissim erat eu leo
											bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenatis tristique. Donec
											convallis aliquetlacus, vitae scelerisque lorem
											et mi. Lorem ipsum dolor sit amet, consecte-
											tur adipiscing elit. Phasellus dignissi erat eu
											leo bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenais tristique. Donec
											convallis aliquet lacus, vitae scelerisque
											lorem et mi.
										</p>
										
										<div class="buttons">
											<a href="#" class="button sm grey">Explore</a>
											<a href="#" class="button sm">Book Now</a>
										</div><!-- .buttons -->
										
									</div><!-- .package -->
								</li>
								<li>
									<div class="package">
									
										<a href="#" class="title">Coastal Adventure Boat Tour</a>
										
										<span class="package-img" data-src="../assets/images/temp/gallery/gallery-4.jpg"></span>
										
										<p>
											Lorem ipsum dolor sit amet, consectetur
											adipiscing elit. Phasellus dignissim erat eu leo
											bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenatis tristique. Donec
											convallis aliquetlacus, vitae scelerisque lorem
											et mi. Lorem ipsum dolor sit amet, consecte-
											tur adipiscing elit. Phasellus dignissi erat eu
											leo bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenais tristique. Donec
											convallis aliquet lacus, vitae scelerisque
											lorem et mi.
										</p>
										
										<div class="buttons">
											<a href="#" class="button sm grey">Explore</a>
											<a href="#" class="button sm">Book Now</a>
										</div><!-- .buttons -->
										
									</div><!-- .package -->
								</li>
								<li>
									<div class="package">
									
										<a href="#" class="title">Sail &amp; Stay Package</a>
										
										<span class="package-img" data-src="../assets/images/temp/gallery/gallery-4.jpg"></span>
										
										<p>
											Lorem ipsum dolor sit amet, consectetur
											adipiscing elit. Phasellus dignissim erat eu leo
											bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenatis tristique. Donec
											convallis aliquetlacus, vitae scelerisque lorem
											et mi. Lorem ipsum dolor sit amet, consecte-
											tur adipiscing elit.
										</p>
										
										<div class="buttons">
											<a href="#" class="button sm grey">Explore</a>
											<a href="#" class="button sm">Book Now</a>
										</div><!-- .buttons -->
										
									</div><!-- .package -->
								</li>
								<li>
									<div class="package">
									
										<a href="#" class="title">Tour, Tasting &amp; Tails</a>
										
										<span class="package-img" data-src="../assets/images/temp/gallery/gallery-4.jpg"></span>
										
										<p>
											Lorem ipsum dolor sit amet, consectetur
											adipiscing elit. Phasellus dignissim erat eu leo
											bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenatis tristique. Donec
											convallis aliquetlacus, vitae scelerisque lorem
											et mi. Lorem ipsum dolor sit amet, consecte-
											tur adipiscing elit. Phasellus dignissi erat eu
											leo bibendum, volutpat iaculis sapien fringilla.
											Morbi imperdiet venenais tristique. Donec
											convallis aliquet lacus, vitae scelerisque
											lorem et mi.
										</p>
										
										<div class="buttons">
											<a href="#" class="button sm grey">Explore</a>
											<a href="#" class="button sm">Book Now</a>
										</div><!-- .buttons -->
										
									</div><!-- .package -->
								</li>
							</ul>
						</div><!-- .tours-drop -->
						
					</li>
					<li><a href="#">Rates &amp; Schedules</a></li>
					<li>
						<a href="#">Getting Here</a>
						<ul>
							<li><a href="#">Maps &amp; Guides</a></li>
							<li><a href="#">Shuttle Service</a></li>
						</ul>
					</li>
					<li><a href="#">Book Now</a></li>
					<li><a href="#">FAQ's</a></li>
					<li><a href="#">The Latest</a></li>
				</ul>
			</nav>
	
			<div class="nav-contact">
			
				<address>
					<span>O'Brien's Whale &amp; Bird Tours</span>
					P.O. Box 300 <br />
					Bay Bulls, NL Canada <br />
					A0A 1C0
				</address>
				
				<span class="phone">
					<span class="label">Telephone</span> 709-753-4850
				</span><!-- .phone -->
				
				<span class="phone">
					<span class="label">Toll Free</span> 1-877-NF-WHALE (639-4253)
				</span><!-- .phone -->
				
				<span class="phone">
					<span class="label">Fax</span> 709-334-2137
				</span><!-- .phone -->
				
				<a href="#" class="button big block">Contact</a>
			
			</div><!-- .nav-contact -->

		</div><!-- .nav-list-wrap -->
		
	</div>
	
</div><!-- .nav -->
<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-puffins-3.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">Things To Do</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-newspaper-o">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="ov-grid">
		
			<a href="#" class="ov-item quarter bounce">
				<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/overview/ov-1.jpg"></div>
				<div class="ov-item-content">
					<span class="ov-title">Puffin and Whale Watching</span>
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at sodales nibh. In bibendum, 
						metus vel faucibus porta, est libero iaculis eros, sit amet porttitor qua m lacus ac diam 
						In bibendum, metus vel faucibus
					</p>
					
					<span class="button">More Info</span>
				</div><!-- .ov-item-content -->
			</a><!-- .ov-item -->
			
			<a href="#" class="ov-item quarter bounce">
				<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/overview/ov-2.jpg"></div>
				<div class="ov-item-content">
					<span class="ov-title">Iceberg Viewing</span>
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at sodales nibh. In bibendum, 
						metus vel faucibus porta, est libero iaculis eros, sit amet porttitor qua m lacus ac diam 
						In bibendum, metus vel faucibus
					</p>
					
					<span class="button">More Info</span>
				</div><!-- .ov-item-content -->
			</a><!-- .ov-item -->
			
			<a href="#" class="ov-item quarter bounce">
				<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/overview/ov-3.jpg"></div>
				<div class="ov-item-content">
					<span class="ov-title">Sailor's Galley Restaurant</span>
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at sodales nibh. In bibendum, 
						metus vel faucibus porta, est libero iaculis eros, sit amet porttitor qua m lacus ac diam 
						In bibendum, metus vel faucibus
					</p>
					
					<span class="button">More Info</span>
				</div><!-- .ov-item-content -->
			</a><!-- .ov-item -->
			
			<a href="#" class="ov-item quarter bounce">
				<div class="ov-item-bg lazybg img" data-src="../assets/images/temp/overview/ov-4.jpg"></div>
				<div class="ov-item-content">
					<span class="ov-title">O'Brien's Gift Shop</span>
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at sodales nibh. In bibendum, 
						metus vel faucibus porta, est libero iaculis eros, sit amet porttitor qua m lacus ac diam 
						In bibendum, metus vel faucibus
					</p>
					
					<span class="button">More Info</span>
				</div><!-- .ov-item-content -->
			</a><!-- .ov-item -->
			
		</div><!-- .ov-grid -->

	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-puffins.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					
					<div class="hero-content">

						<h1 class="hero-title">Half a Million Atlantic Puffins</h1>
						
						<div class="hero-hr">
							<span class="obriens-f ob-puffins">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Home to the largest Atlantic puffin colony in North America</span>
						
						<a href="#" class="button big outline">Take Me There</a>

					</div><!-- .hero-content -->

				</div><!-- .swipe-item -->
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-search.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					
					<div class="hero-content">

						<h1 class="hero-title">2 Half a Million Atlantic Puffins</h1>
						
						<div class="hero-hr">
							<span class="obriens-f ob-icebergs">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Home to the largest Atlantic puffin colony in North America</span>
						
						<a href="#" class="button big outline">Take Me There</a>

					</div><!-- .hero-content -->

				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<!-- MAIN BODY CONTENT -->
	<div class="body">
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
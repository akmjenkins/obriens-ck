<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero">
		<div class="swiper-wrapper">
			<div class="swiper"
				data-infinite="true" 
				data-arrows="false" 
				data-autoplay="true"
				data-autoplay-speed="7000"
				data-pause-on-hover="false"
				data-update-lazy-images="true" 
				data-fade="true">
				
				<div class="swipe-item">
					<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-puffins-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
					<div class="hero-content">

						<h1 class="hero-title">The Latest</h1>
						
						<div class="hero-hr">
							<span class="t-fa-abs fa-newspaper-o">&nbsp;</span>
						</div><!-- .hero-hr -->
						
						<span class="hero-subtitle">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</span>

					</div><!-- .hero-content -->
				</div><!-- .swipe-item -->
				
			</div><!-- .swiper -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->
	
	<div class="body">
	
		<div class="pad-20">
	
			<div class="grid pad40">
			
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="article-body">
						
							<div class="hgroup">
								<h3>Donec a magna enim. Aliquam sollicitudin ex sit amet tristique semper. Integer eu nunc ornare.</h3>
							</div><!-- .hgroup -->
							
							<time datetime="2015-05-20" pubdate>
								May 20
								<span class="year">2015</span>
							</time>
							
							<p>
								This is a take-your-breath-away outdoor adventure, topped off with a wink-of-the-eye sense of fun. 
								Come for the whales, birds and the O'Brien's stories and insights into the wonders of this marine treasure.
							</p>
							
							<div class="lazybg full-img">
								<img src="../assets/images/temp/article-body.jpg" class="block" alt="iceberg">	
							</div><!-- .lazybg -->
							
							<p>
								Join us for a two-hour cruise to the Witless Bay Ecological Reserve for a nautical adventure of a lifetime. 
								Our 50-foot boat has rail space for everyone's viewing excitement. See whales, wheeling seabirds and soaring 
								eagles, nothing compares to the thrill of meeting the wild world in such a personal way.
							</p>
							
							<a href="#" class="button">Read More</a>
							
						</div><!-- .article-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="article-body">
						
							<div class="hgroup">
								<h3>Donec a magna enim. Aliquam sollicitudin ex sit amet tristique semper. Integer eu nunc ornare.</h3>
							</div><!-- .hgroup -->
							
							<time datetime="2015-05-20" pubdate>
								May 20
								<span class="year">2015</span>
							</time>
							
							<p>
								This is a take-your-breath-away outdoor adventure, topped off with a wink-of-the-eye sense of fun. 
								Come for the whales, birds and the O'Brien's stories and insights into the wonders of this marine treasure.
							</p>
							
							<div class="lazybg full-img">
								<img src="../assets/images/temp/article-body.jpg" class="block" alt="iceberg">	
							</div><!-- .lazybg -->
							
							<p>
								Join us for a two-hour cruise to the Witless Bay Ecological Reserve for a nautical adventure of a lifetime. 
								Our 50-foot boat has rail space for everyone's viewing excitement. See whales, wheeling seabirds and soaring 
								eagles, nothing compares to the thrill of meeting the wild world in such a personal way.
							</p>
							
							<a href="#" class="button">Read More</a>
							
						</div><!-- .article-body -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
	
		</div>
	
	</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>
;(function(context) {
	
	var 
		$docuemnt,
		debounce,
		methods,
		imageLoader;
	
	if(context) {
		imageLoader = context.imageLoader;
	} else {
		imageLoader = require('./image.loader.js');
	}
	
	$document = $(document);
	
	methods = {
		
		getElementWithSrcData: function(el) {
			return el.data('src') !== undefined ? el : el.find('.swipe-item-bg').filter(function() { return $(this).data('src') !== undefined });
		},
		
		setImageOnElements: function(els,source) {
			els.each(function() {
				$(this)
					.css({backgroundImage: 'url('+source+')' })
					.addClass('loaded');
			});
		},
		
		loadImageForElementAtIndex: function(i,el) {
			var 
				self = this,
				element = $('.swipe-item',el).filter(function() { return !$(this).hasClass('slick-cloned'); }).eq(i),
				sourceElement = this.getElementWithSrcData(element),
				rawSource = sourceElement.data('src'),
				source = imageLoader.getAppropriateSource(rawSource),
				allElements = sourceElement.add(self.getElementWithSrcData(element.siblings()).filter(function() { 
					return $(this).data('src') === rawSource; 
				}));
			
				element.addClass('loading');
				
				if(!source) {
					element.addClass('loaded');
				}
				
				if(imageLoader.hasSourceLoaded(source)) {
					this.setImageOnElements(allElements,source);
					return;
				}
				
				imageLoader
					.loadSource(source)
					.then(function(source) {
						self.setImageOnElements(allElements,source);
						element.addClass('loaded');
					});
			
		},
	
		getSwiperWrappers: function() {
			return $('div.swiper-wrapper');
		},
	
		buildSwiper: function(wrapper,options) {
			
			var 
				settings = options || {},
				el = wrapper,
				swiper = el.children('div.swiper'),
				selectNav = el.find('select.swiper-nav'),
				responsive = swiper.data('responsive');
				
				$.extend(settings,swiper.data());
				
			el.data('slick',swiper.slick(settings));
			
			selectNav.on('change',function() { swiper.slick('goTo',this.selectedIndex); });
			
			settings.slidesToShow = settings.slidesToShow || 1;
			
			if(settings.updateLazyImages) {
				swiper.on('beforeChange',function(slick,e,current,next) {					
					for(var i = 0; i < (swiper.slick('slickGetOption','slidesToShow')+1); i++) {
						methods.loadImageForElementAtIndex(next+i,swiper);	
					}
				});
				
				for(var i = 0; i< (swiper.slick('slickGetOption','slidesToShow')+1); i++) {
					methods.loadImageForElementAtIndex(swiper.slick('slickCurrentSlide')+i,swiper);	
				}
			}
			
		},
		
		updateTemplate: function() {
			methods
				.getSwiperWrappers()
				.filter(function() {
					return typeof $(this).data('slick') === 'undefined'
				})
				.each(function() {
					methods.buildSwiper($(this));
				});		
		}
	
	}
	
	$document
		.on('updateTemplate.swiper ready',function(e) {
			methods.updateTemplate();
		});
		
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = {
			buildSwiper: function(wrapper,options) { methods.buildSwiper(wrapper,options); }
		}
	//CodeKit
	} else if(context) {
		context.swiper = {
			buildSwiper: function(wrapper,options) { methods.buildSwiper(wrapper,options); }
		}
	}	

}(typeof ns !== 'undefined' ? window[ns] : undefined));
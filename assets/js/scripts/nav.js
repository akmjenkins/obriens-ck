;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		methods,
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$navListWrap = $('.nav-list-wrap'),
		$nav = $('nav'),
		$navContact = $('div.nav-contact'),
		SHOW_CLASS = 'show-nav',
		COLLAPSE_NAV_AT = 800,
		navListWrapTop = $navListWrap.offset().top,
		navListWrapHeight = $navListWrap.get(0).getBoundingClientRect().height,
		navHeight = $nav.get(0).getBoundingClientRect().height,
		navTop = $nav.offset().top,
		navContactHeight = $nav.get(0).getBoundingClientRect().height,
		navContactTop = $navContact.offset().top;

	methods = {
	
		toggleNavOverlay: function() {
			$html.toggleClass(SHOW_NAV_OVERLAY_CLASS);
		},
		
		isNavCollapsed: function() {
			return window.innerWidth < COLLAPSE_NAV_AT;
		},
		
		shouldDoOffset: function() {
			return window.innerWidth > COLLAPSE_NAV_AT;
		},
		
		reset: function() {
			var offset = 0;
			
			$navListWrap
				.add($navContact)
				.css({
					'-webkit-transform':'translate3d(0,'+offset+'px, 0)',
					'-moz-transform':'translate3d(0,'+offset+'px, 0)',
					'-ms-transform':'translate3d(0,'+offset+'px, 0)',
					'transform':'translate3d(0,'+offset+'px, 0)'
				});
		
		},
		
		setTransformY: function() {
			var offset = 0,$el,top,referenceTop = document.documentElement.scrollTop || document.body.scrollTop;
			
			if(this.shouldDoOffset()) {
				
				if(window.innerHeight > navListWrapHeight) {
					$el = $navListWrap;
					top = navListWrapTop
				} else {
					$el = $navContact;
					top = navContactTop;
				}
				if(referenceTop > top) {
					offset = referenceTop - top;
				}
				
			} else {
				
				$el = $navListWrap.add($navContact);
				
			}
			
			$el.css({
				'-webkit-transform':'translate3d(0,'+offset+'px, 0)',
				'-moz-transform':'translate3d(0,'+offset+'px, 0)',
				'-ms-transform':'translate3d(0,'+offset+'px, 0)',
				'transform':'translate3d(0,'+offset+'px, 0)'
			});
				
		},
		
		onScroll: function() {
			this.setTransformY();
		},
		
		onResize: function() {
			this.reset();
			this.setTransformY();
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		}

	};
	
	//listeners
	$document
		.on('mouseenter','.tours-link',function() {
			$('.package-img').addClass('lazybg');
			setTimeout(function() {
				$(document).trigger('updateTemplate.lazyimages');
			},500);
		})
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.showNav(false);
		})		
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		});
		
	$window
		.on('scroll',function() { methods.onScroll(); })
		.on('resize',function() { methods.onResize(); });
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));
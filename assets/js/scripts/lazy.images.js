;(function(context) {

	var debounce,imageLoader;
	
	if(context) {
		debounce = context.debounce;
		imageLoader = context.imageLoader;
	} else {
		debounce = require('./debounce.js');
		imageLoader = require('./image.loader.js');
	}

	var
		d = debounce(),
		$window = $(window),
		$document = $(document),
		windowHeight = window.innerHeight;
		updateHeight = false,
		methods = {
		
			getImages: function() {
				return $('.lazybg').filter(function() { return $(this).parent().is(':visible') || $(this).hasClass('force') });
			},
		
			updateLazyImages: function() {
				if(updateHeight) {
					windowHeight = window.innerHeight;
				}
				var topOfLazyImage = $window.scrollTop()+windowHeight;
				
				this.loadImages(methods.getImages().filter(function() {
						var el = $(this);
						return el.hasClass('loading') ? false : (el.offset().top-topOfLazyImage < 0 || el.hasClass('immediate'));
					})
				);
				
			},
			
			setBackgroundImageOnEl: function(el,img) {
				el
					.css({backgroundImage:'url('+img+')'})
					.addClass('loaded')
					.removeClass('loading');
			},
		
			loadImages: function(images) {
				var self = this;
				
				$.each(images,function() {
					var 
						childImg,
						img = $(this),
						source = imageLoader.getAppropriateSource(img.data('src'));
					if(!source) {
						img.find('img').each(function() {
							source = this.src;
							return false;
						});
					}
					
					//this image has already been loaded and can be displayed immediately
					if(imageLoader.hasSourceLoaded(source)) {
						self.setBackgroundImageOnEl(img,source);
						return;
					}
						
					if(source) {
						img.addClass('loading');
						imageLoader
							.loadSource(source)
							.then(function() {
								self.setBackgroundImageOnEl(img,source);
							});
					}
						
				});
				
			}		
		};
	
	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.lazyimages'); or just $(document).trigger('updateTemplate');
	$window.on('scroll load resize updateTemplate.lazyimages',function(e) { 
		upateHeight = e.type === 'resize';
		d.requestProcess(function() { methods.updateLazyImages(); }); 
	}).trigger('updateTemplate.lazyimages');

	//no public API
	return {};
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));